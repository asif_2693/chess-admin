import { Component, ViewChild, AfterViewInit , OnInit} from '@angular/core';

import { ChessBoard, PieceColor, PlayerType } from '../../../ngx-chess/src/ngx-chess';
import { CHESSJS_CHESS_GAME_PROVIDERS } from '../../../ngx-chess/plugins/chessjs/src/chessjs';
import { Router } from '@angular/router';

@Component({
  templateUrl: 'dashboard.component.html',
  providers: [ ...CHESSJS_CHESS_GAME_PROVIDERS ],
})
export class DashboardComponent implements AfterViewInit {

  // ngOnInit(): void { }

  @ViewChild('board') private board: ChessBoard;

  constructor() {}


  ngAfterViewInit() {
    this.board.ctrl
      .init()
      .then( () => {
        this.board.ctrl
          .setPlayer(PieceColor.BLACK, PlayerType.HUMAN)
          .setPlayer(PieceColor.WHITE, PlayerType.HUMAN)
          .newGame();
      });
  }
}
